package pl.gk.example.jpa.cascade.delete.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.gk.example.jpa.cascade.delete.JpaCascadeDeleteApplication;

@SpringBootTest(classes = JpaCascadeDeleteApplication.class)
class MyServiceTest {

  @Autowired
  private MyService myService;

  @Test
  public void testDocument() {
//    Long id = myService.testDocument();
//    myService.testDeleteDocument(id);

    Long id = myService.addDocument();
    myService.addView(id);
    myService.testDeleteDocument(id);
  }
}