package pl.gk.example.jpa.cascade.delete;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = {"pl.gk.example.jpa.cascade.delete"})
@ComponentScan({"pl.gk.example.jpa.cascade.delete"})
@EntityScan("pl.gk.example.jpa.cascade.delete.model")
public class JpaCascadeDeleteApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaCascadeDeleteApplication.class, args);
	}

}
