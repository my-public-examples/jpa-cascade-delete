package pl.gk.example.jpa.cascade.delete.service;

import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.gk.example.jpa.cascade.delete.model.DocumentEntity;
import pl.gk.example.jpa.cascade.delete.model.ViewEntity;
import pl.gk.example.jpa.cascade.delete.repository.DocumentEntityRepository;

@Service
@Transactional
public class MyService {

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private DocumentEntityRepository documentEntityRepository;

  public Long testDocument() {
    DocumentEntity doc = new DocumentEntity();
    doc.setUuid(UUID.randomUUID().toString());
    ViewEntity view = new ViewEntity();
    view.setName("Test");
    doc.setView(view);
    em.persist(doc);
    return doc.getId();
  }

  public Long addDocument() {
    DocumentEntity doc = new DocumentEntity();
    doc.setUuid(UUID.randomUUID().toString());
    em.persist(doc);
    return doc.getId();
  }

  public void addView(Long id) {
    DocumentEntity doc = em.find(DocumentEntity.class, id);
    ViewEntity view = new ViewEntity();
    view.setName("Test");
    doc.setView(view);
    em.persist(doc);
  }

  public void testDeleteDocument(Long id) {
    DocumentEntity dd = em
        .createQuery("from DocumentEntity d where d.id = :id",
            DocumentEntity.class)
        .setParameter("id", id)
        .getSingleResult();
    if (dd != null) {
      em.remove(dd);
    }
  }
}
