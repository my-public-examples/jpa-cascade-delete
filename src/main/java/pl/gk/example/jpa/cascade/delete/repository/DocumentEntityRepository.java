package pl.gk.example.jpa.cascade.delete.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.gk.example.jpa.cascade.delete.model.DocumentEntity;

@Repository
public interface DocumentEntityRepository extends JpaRepository<DocumentEntity, Long> {
}
